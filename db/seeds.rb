# frozen_string_literal: true

User.delete_all
Applicant.delete_all
Course.delete_all
Department.delete_all
County.delete_all

User.create!(email: 'admin@admin.com', password: 'admin1234')
User.create!(email: 'linda@lee.com', password: 'blablablah')

puts 'Creating department... '
departments = ['computing',
               'engineering',
               'tailoring',
               'capentry',
               'bascomy',
               'hommosapiens',
               'husbandry',
               'data-analysis']
departments.each do |department|
  Department.create!(department_name: department)
end

puts 'Creating courses.... '
courses = %w[computing engineering tailoring capentry bascomy hommosapiens husbandry data-analysis]
courses.each do |course|
  Course.create!(course_name: course, department: Department.all.sample)
end
puts 'Creating Counties...'
counties = %w[Baringo Bomet Bungoma Busia Elgeyo-Marakwet Embu Garissa Homa Bay Isiolo Kajiado Kakamega Kericho Kiambu Kilifi Kirinyaga Kisii Kisumu Kitui Kwale Laikipia Lamu Machakos Makueni Mandera Marsabit Meru Migori Mombasa Muranga Nairobi Nakuru Nandi Narok Nyamira Nyandarua Nyeri Samburu Siaya Taita-Taveta Tana-River Tharaka-Nithi Trans-Nzoia Turkana Uasin-Gishu Vihiga Wajir West-Pokot]
counties.each do |county|
  County.create!(county_name: county)
end

puts 'Creating Applicants...'

100.times do
  first_name = Faker::Name.first_name
  last_name = Faker::Name.last_name
  dob = Faker::Date.birthday(18, 65)
  id_number = Faker::Number.unique.number(8)
  mobile = Faker::Number.unique.number(10)
  email = Faker::Internet.unique.email
  parent_name = Faker::Name.name
  address = Faker::Address.community
  town = Faker::Address.city
  fee_payer = %w[Parent/Guardian Self Sponsor].sample
  admission_time = %w[January May September].sample
  transaction_code = Faker::Number.unique.number(8)
  employment_status = %w[No Yes SelfEmployed].sample
  kcpe_year = Faker::Number.between(1955, 2018)
  kcse_year = Faker::Number.between(1955, 2018)
  kcpe_marks = Faker::Number.between(100, 500)
  kcse_grade = Applicant::GRADES.sample
  kcse_index_number = Faker::Number.number(8)

  Applicant.create!(
    first_name: first_name,
    last_name: last_name,
    date_of_birth: dob,
    id_number: id_number,
    mobile: mobile,
    email: email,
    parent_name: parent_name,
    address: address,
    town: town,
    fee_payer: fee_payer,
    admission_time: admission_time,
    transaction_code: transaction_code,
    employment_status: employment_status,
    kcpe_year: kcpe_year,
    kcpe_marks: kcpe_marks,
    kcse_year: kcse_year,
    kcse_grade: kcse_grade,
    kcse_index_number: kcse_index_number,
    county: County.all.sample,
    course: Course.all.sample
  )
end
