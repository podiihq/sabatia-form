require 'rails_helper'

RSpec.describe County, type: :model do
  describe "should have db column" do
    it{is_expected.to have_db_column(:county_name)}
    it{should have_many(:applicants)}
  end
end
