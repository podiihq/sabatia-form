class CreateApplicants < ActiveRecord::Migration[5.2]
  def change
    create_table :applicants do |t|
      t.string :first_name
      t.string :last_name
      t.datetime :date_of_birth
      t.integer :id_number
      t.string :mobile
      t.string :email
      t.string :parent_name
      t.string :address
      t.string :town
      t.string :fee_payer
      t.string :admission_time
      t.string :employment_status
      t.integer :kcpe_year
      t.integer :kcse_year
      t.integer :kcpe_marks
      t.string :kcse_grade
      t.integer :kcse_index_number
      t.string :transaction_code

      t.timestamps
    end
  end
end
