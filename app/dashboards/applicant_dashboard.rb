require "administrate/base_dashboard"

class ApplicantDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    course: Field::BelongsTo,
    county: Field::BelongsTo,
    id: Field::Number,
    first_name: Field::String,
    last_name: Field::String,
    date_of_birth: Field::DateTime,
    id_number: Field::Number,
    mobile: Field::String,
    kcpe_year: Field::Number,
    kcpe_marks: Field::Number,
    kcse_year: Field::Number,
    kcse_grade: Field::String,
    email: Field::String,
    parent_name: Field::String,
    address: Field::String,
    town: Field::String,
    fee_payer: Field::String,
    admission_time: Field::String,
    employment_status: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :first_name,
    :course,
    :county,
    :kcse_grade,
    :admission_time
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :course,
    :county,
    :first_name,
    :last_name,
    :date_of_birth,
    :id_number,
    :mobile,
    :email,
    :parent_name,
    :address,
    :town,
    :fee_payer,
    :admission_time,
    :employment_status,
    :kcse_year,
    :kcse_grade,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :course,
    :county,
    :first_name,
    :last_name,
    :date_of_birth,
    :id_number,
    :mobile,
    :email,
    :parent_name,
    :address,
    :town,
    :fee_payer,
    :admission_time,
    :employment_status,
  ].freeze

  # Overwrite this method to customize how applicants are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(applicant)
    "#{applicant.first_name} #{applicant.last_name}"
  end
end
