require 'rails_helper'

RSpec.describe Department, type: :model do
  describe "validations" do
    it{ should have_db_column(:department_name)}
    it{is_expected.to validate_presence_of(:department_name)}
    it{ should have_many(:courses) }
  end
end
