# frozen_string_literal: true

class Applicant < ApplicationRecord
  GRADES = %w( A A- B+ B B- C+ C C- D+ D D- E  )
  belongs_to :course
  belongs_to :county

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :date_of_birth, presence: true
  VALID_EMAIL_REGEX = /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9\.-]+\.[A-Za-z]+\Z/
  validates :email, presence: true, uniqueness: true, format: { with: VALID_EMAIL_REGEX }
  validates :id_number, presence: true, uniqueness: true, length: { minimum: 5, maximum: 10 }
  validates :mobile, presence: true, uniqueness: true
  validates :parent_name, :address, :town, :employment_status, :fee_payer, :admission_time, presence: true
  validates :kcpe_year, :kcse_year, presence: true, length: { maximum: 4}
  validates :kcpe_marks, presence: true, length: {minimum: 3}
  validates :kcse_index_number, :transaction_code, presence: true
end
