class AddCourseToApplicant < ActiveRecord::Migration[5.2]
  def change
    add_reference :applicants, :course, foreign_key: true
  end
end
