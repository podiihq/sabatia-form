require 'rails_helper'

RSpec.describe Applicant, type: :model do
  describe "should have valid db columns" do
    it{is_expected.to have_db_column(:first_name)}
    it{is_expected.to have_db_column(:last_name)}
    it{is_expected.to have_db_column(:id)}
    it{is_expected.to have_db_column(:date_of_birth)}
    it{is_expected.to have_db_column(:id_number)}
    it{is_expected.to have_db_column(:mobile)}
    it{is_expected.to have_db_column(:email)}
    it{is_expected.to have_db_column(:parent_name)}
    it{is_expected.to have_db_column(:address)}
    it{is_expected.to have_db_column(:town)}
    it{is_expected.to have_db_column(:employment_status)}
    it{is_expected.to have_db_column(:fee_payer)}
    it{is_expected.to have_db_column(:admission_time)}
    it{is_expected.to have_db_column(:kcpe_year)}
    it{is_expected.to have_db_column(:kcpe_marks)}
    it{is_expected.to have_db_column(:kcse_year)}
    it{is_expected.to have_db_column(:kcse_grade)}
    it{is_expected.to have_db_column(:kcse_index_number)}
    it{is_expected.to have_db_column(:transaction_code)}
  end
  describe "validations" do
    context "Validate Presence" do
      it { is_expected.to validate_presence_of(:first_name) }
      it { is_expected.to validate_presence_of(:last_name) }
      it { is_expected.to validate_presence_of(:date_of_birth) }
      it { is_expected.to validate_presence_of(:email) }
      it { is_expected.to validate_presence_of(:mobile)}
      it { is_expected.to validate_presence_of(:id_number)}
      it { is_expected.to validate_presence_of(:transaction_code)}
      it { is_expected.to validate_presence_of(:kcpe_year)}
      it { is_expected.to validate_presence_of(:kcpe_marks)}
      it { is_expected.to validate_presence_of(:kcse_year)}
      it { is_expected.to validate_presence_of(:kcse_index_number)}
    end

    context "validate uniqueness" do
      it { is_expected.to validate_uniqueness_of(:email)}
      it{should validate_uniqueness_of(:id_number)}
      it{should validate_uniqueness_of(:mobile)}
    end

    context "validate email format" do
      it { is_expected.to allow_value("user@example.com").for(:email)}
      it { is_expected.to allow_value("user123@example.com").for(:email)}
      it { is_expected.to_not allow_value("user@example,com").for(:email)}
    end
    context "validate associations" do
      it {should belong_to(:county)}
    end
  end
end
