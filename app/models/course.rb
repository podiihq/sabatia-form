class Course < ApplicationRecord
  has_many :applicants
  belongs_to :department

  validates :course_name, presence: true
end
