class County < ApplicationRecord
  has_many :applicants
  validates :county_name, presence: true
end
