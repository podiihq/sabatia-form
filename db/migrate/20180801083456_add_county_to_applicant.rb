class AddCountyToApplicant < ActiveRecord::Migration[5.2]
  def change
    add_reference :applicants, :county, foreign_key: true
  end
end
