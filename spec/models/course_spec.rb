# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Course, type: :model do
  describe 'validations' do
    it { should have_db_column(:course_name) }
    it { should validate_presence_of(:course_name) }
    it { should have_many(:applicants) }
    it { should belong_to(:department) }
  end
end
