class Department < ApplicationRecord
  has_many :courses
  validates :department_name, presence: true
end
