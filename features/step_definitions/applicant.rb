Given("I am on the application page") do
  visit '/applicants/new?checkMe=i+have+read+instructions'
end

Then("I should see {string}") do |string|
  expect(page).to have_content string
end
Then("I fill in {string} with {string}") do |field, value|
  fill_in field, with: value
end
When("I click submit") do
  click_button "submit"
end
When("I click radio-button {string}") do |string|
  choose string
end
Then("I select {string} from {string}") do |county, county_options|
  select county, from: county_options
end

Then("show me the page") do
  save_and_open_page
end

Given("the following counties exist") do |table|
  table.hashes.each do |county|
    County.create!(county_name: county["county_name"])
 end
end

Given("the following courses exist") do |table|
  table.hashes.each do |course|
    department = Department.find_by_department_name course["department"]
    Course.create!(course_name: course["course_name"], department: department)
  end
end

Given("the following departments exist") do |table|
  table.hashes.each do |department|
    Department.create!(department_name: department["department_name"])
  end
end
