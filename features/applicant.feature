Feature:
  As an applicant
  So that I can be able to apply
  I need to submit my details
  Background:
    Given the following counties exist
      | county_name |
      | Kisumu      |
      | Bungoma     |

    Given the following departments exist
      | department_name |
      | Physics         |
      | Maths           |

    And the following courses exist
      | course_name | department |
      | Electrical  | Physics    |
      | Mechanical  | Maths      |

  Scenario: Successful Form Submission
    Given I am on the application page
    Then I fill in "applicant_first_name" with "Getty"
    And I fill in "applicant_last_name" with "Orawo"
    And I fill in "applicant_date_of_birth" with "26/11/2018"
    And I fill in "applicant_mobile" with "254711703303"
    And I fill in "applicant_email" with "getty@gmail.org"
    And I fill in "applicant_id_number" with "31391624"
    And I fill in "applicant_address" with "63632"
    And I fill in "applicant_town" with "Kisii"
    And I fill in "applicant_parent_name" with "Doreen"
    And I fill in "applicant_kcpe_year" with "1990"
    And I fill in "applicant_kcse_year" with "1995"
    And I fill in "applicant_kcpe_marks" with "344"
    And I fill in "applicant_transaction_code" with "QWERTY344U"
    And I select "A" from "applicant_kcse_grade"
    And I fill in "applicant_kcse_index_number" with "503209922"
    And I select "Kisumu" from "applicant_county_id"
    Then show me the page
    And I select "Electrical" from "applicant_course_id"
    And I click radio-button "applicant_employment_status_no"
    And I click radio-button "applicant_fee_payer_self"
    And I click radio-button "applicant_admission_time_may"
    When I click submit
    Then I should see "Getty"
    And I should see "Orawo"
    And I should see "254711703303"

  Scenario: Unsuccessful application
    Given I am on the application page
    When I fill in "applicant_first_name" with " "
    And I fill in "applicant_last_name" with " "
    And I fill in "applicant_date_of_birth" with " "
    And I fill in "applicant_id_number" with " "
    And I fill in "applicant_mobile" with " "
    And I fill in "applicant_email" with " "
    And I fill in "applicant_parent_name" with " "
    And I fill in "applicant_address" with " "
    And I fill in "applicant_town" with " "
    And I click submit
    Then I should see "First name can't be blank"
    And I should see "Last name can't be blank"
    And I should see "Date of birth can't be blank"
    And I should see "Id number can't be blank"
    And I should see "Mobile can't be blank"
    And I should see "Email can't be blank"
