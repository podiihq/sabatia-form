Rails.application.routes.draw do
  namespace :admin do
    resources :applicants
    resources :counties
    resources :courses
    resources :departments

    root to: "applicants#index"
  end
  root to: 'applicants#instructions'
  resources :applicants, only: [:new, :show, :create]
  devise_for :users
end
