# frozen_string_literal: true

class ApplicantsController < ApplicationController
  def new
    read_instructions = params['checkMe']
    redirect_to root_path unless read_instructions
    @applicant = Applicant.new
  end

  def instructions
  end

  def create
    @applicant = Applicant.new(applicant_params)
    if @applicant.save
      flash[:success] = 'You have successfully submitted your application'
      redirect_to @applicant
    else
      render 'new'
    end
  end

  def show
    @applicant = Applicant.find(params[:id])
  end

  private

  def applicant_params
    params.require(:applicant).permit(:first_name,
                                      :last_name,
                                      :date_of_birth,
                                      :id_number,
                                      :mobile,
                                      :email,
                                      :parent_name,
                                      :address,
                                      :town,
                                      :employment_status,
                                      :fee_payer,
                                      :admission_time,
                                      :county_id,
                                      :course_id,
                                      :kcpe_year,
                                      :kcse_year,
                                      :kcpe_marks,
                                      :kcse_grade,
                                      :kcse_index_number,
                                      :transaction_code
                                     )
  end
end
